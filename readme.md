# Dynamic DNS

[MyDNS](https://www.mydns.jp/)用のCronJob

[さくら](https://secure.sakura.ad.jp/menu/domain/?)が`slimemoss.com`のレジストラです。

# クイックスタート
```
sed -e "s/MYDNS_PASS/***/g" mydns.yaml | kubectl apply -f -
```
